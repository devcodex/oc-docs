### Postgres Database setup for OC Pizzarias
Console install:
* To open postgres shell run the command ```psql --username=postgres```
* inside the shell run```CREATE DATABASE [database];``` to create the database
* Use ```\q``` to quit the shell
* Populate the database by runnning ```psql --username=[username] -d [database] -f fill_script.sql``` (replace ```[username]``` and ```[database]``` with your own values )

pgAdmin install (this tutoria is for version pgAdmin v4):
* Open your instance of pgAdmin
* Create a new database (Server tree: Server > postgres > database, right-click database and select Create > Database)

![tut_img1](https://gitlab.com/devcodex/oc-docs/raw/master/P5/pgAdmin_images/create.JPG)

* Right-click schema ```public``` and select ```Query tool...```

![tut_img3](https://gitlab.com/devcodex/oc-docs/raw/master/P5/pgAdmin_images/launch_sql_commands.JPG)

* Copy the contents of file ```1_create_tables.sql```, paste them on the query field and run it

![tut_img3](https://gitlab.com/devcodex/oc-docs/raw/master/P5/pgAdmin_images/table_creation.JPG)

* Repeat the previous action, but this time with the file ```2_data_fill.sql``` to fill the database with data