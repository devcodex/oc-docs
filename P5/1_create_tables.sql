--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5
-- Dumped by pg_dump version 11.2

-- Started on 2019-04-08 19:29:41

--
-- TOC entry 3 (class 2615 OID 29090)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--
DROP SCHEMA IF EXISTS public CASCADE;
CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 29093)
-- Name: address; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE address (
    id integer NOT NULL,
    address text,
    city character varying(255),
    postal_code character varying(255)
);


ALTER TABLE address OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 29091)
-- Name: address_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE address_id_seq OWNER TO postgres;

--
-- TOC entry 2309 (class 0 OID 0)
-- Dependencies: 196
-- Name: address_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE address_id_seq OWNED BY address.id;


--
-- TOC entry 199 (class 1259 OID 29104)
-- Name: bank_transaction; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE bank_transaction (
    id integer NOT NULL,
    date timestamp without time zone,
    details text,
    id_transaction text,
    name character varying(255),
    value double precision,
    order_id integer
);


ALTER TABLE bank_transaction OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 29102)
-- Name: bank_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE bank_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE bank_transaction_id_seq OWNER TO postgres;

--
-- TOC entry 2310 (class 0 OID 0)
-- Dependencies: 198
-- Name: bank_transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE bank_transaction_id_seq OWNED BY bank_transaction.id;


--
-- TOC entry 201 (class 1259 OID 29115)
-- Name: client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE client (
    id integer NOT NULL,
    email character varying(255),
    name character varying(255),
    phone character varying(255),
    address_id integer NOT NULL
);


ALTER TABLE client OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 29113)
-- Name: client_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE client_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE client_id_seq OWNER TO postgres;

--
-- TOC entry 2311 (class 0 OID 0)
-- Dependencies: 200
-- Name: client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE client_id_seq OWNED BY client.id;


--
-- TOC entry 203 (class 1259 OID 29126)
-- Name: employee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE employee (
    id integer NOT NULL,
    email character varying(255),
    is_admin boolean NOT NULL,
    name character varying(255),
    password character varying(255),
    username character varying(255)
);


ALTER TABLE employee OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 29124)
-- Name: employee_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE employee_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE employee_id_seq OWNER TO postgres;

--
-- TOC entry 2312 (class 0 OID 0)
-- Dependencies: 202
-- Name: employee_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE employee_id_seq OWNED BY employee.id;


--
-- TOC entry 205 (class 1259 OID 29137)
-- Name: ingredient; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ingredient (
    id integer NOT NULL,
    description text,
    ingredient_type integer,
    name character varying(255),
    price double precision
);


ALTER TABLE ingredient OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 29135)
-- Name: ingredient_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ingredient_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ingredient_id_seq OWNER TO postgres;

--
-- TOC entry 2313 (class 0 OID 0)
-- Dependencies: 204
-- Name: ingredient_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ingredient_id_seq OWNED BY ingredient.id;


--
-- TOC entry 207 (class 1259 OID 29148)
-- Name: ingredient_recipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ingredient_recipe (
    id integer NOT NULL,
    quantity integer NOT NULL,
    ingredient_id integer,
    recipe_id integer
);


ALTER TABLE ingredient_recipe OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 29146)
-- Name: ingredient_recipe_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ingredient_recipe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ingredient_recipe_id_seq OWNER TO postgres;

--
-- TOC entry 2314 (class 0 OID 0)
-- Dependencies: 206
-- Name: ingredient_recipe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ingredient_recipe_id_seq OWNED BY ingredient_recipe.id;


--
-- TOC entry 209 (class 1259 OID 29156)
-- Name: ingredient_restaurant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE ingredient_restaurant (
    id integer NOT NULL,
    standby_quantity integer NOT NULL,
    stock integer NOT NULL,
    stock_min integer NOT NULL,
    ingredient_id integer,
    restaurant_id integer
);


ALTER TABLE ingredient_restaurant OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 29154)
-- Name: ingredient_restaurant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE ingredient_restaurant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE ingredient_restaurant_id_seq OWNER TO postgres;

--
-- TOC entry 2315 (class 0 OID 0)
-- Dependencies: 208
-- Name: ingredient_restaurant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE ingredient_restaurant_id_seq OWNED BY ingredient_restaurant.id;


--
-- TOC entry 211 (class 1259 OID 29164)
-- Name: order_cart; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE order_cart (
    id integer NOT NULL,
    details text,
    status integer,
    client_id integer NOT NULL,
    restaurant_id integer
);


ALTER TABLE order_cart OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 29162)
-- Name: order_cart_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_cart_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_cart_id_seq OWNER TO postgres;

--
-- TOC entry 2316 (class 0 OID 0)
-- Dependencies: 210
-- Name: order_cart_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_cart_id_seq OWNED BY order_cart.id;


--
-- TOC entry 213 (class 1259 OID 29175)
-- Name: order_item; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE order_item (
    id integer NOT NULL,
    order_id integer,
    recipe_id integer NOT NULL
);


ALTER TABLE order_item OWNER TO postgres;

--
-- TOC entry 212 (class 1259 OID 29173)
-- Name: order_item_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_item_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_item_id_seq OWNER TO postgres;

--
-- TOC entry 2317 (class 0 OID 0)
-- Dependencies: 212
-- Name: order_item_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_item_id_seq OWNED BY order_item.id;


--
-- TOC entry 215 (class 1259 OID 29183)
-- Name: order_modification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE order_modification (
    id integer NOT NULL,
    quantity integer NOT NULL,
    ingredient_id integer,
    order_item_id integer
);


ALTER TABLE order_modification OWNER TO postgres;

--
-- TOC entry 214 (class 1259 OID 29181)
-- Name: order_modification_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE order_modification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE order_modification_id_seq OWNER TO postgres;

--
-- TOC entry 2318 (class 0 OID 0)
-- Dependencies: 214
-- Name: order_modification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE order_modification_id_seq OWNED BY order_modification.id;


--
-- TOC entry 217 (class 1259 OID 29191)
-- Name: recipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE recipe (
    id integer NOT NULL,
    cooking_notes text,
    description text,
    image text,
    name character varying(255),
    price double precision
);


ALTER TABLE recipe OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 29189)
-- Name: recipe_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE recipe_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE recipe_id_seq OWNER TO postgres;

--
-- TOC entry 2319 (class 0 OID 0)
-- Dependencies: 216
-- Name: recipe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE recipe_id_seq OWNED BY recipe.id;


--
-- TOC entry 219 (class 1259 OID 29202)
-- Name: restaurant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE restaurant (
    id integer NOT NULL,
    name character varying(255),
    address_id integer NOT NULL
);


ALTER TABLE restaurant OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 29200)
-- Name: restaurant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE restaurant_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE restaurant_id_seq OWNER TO postgres;

--
-- TOC entry 2320 (class 0 OID 0)
-- Dependencies: 218
-- Name: restaurant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE restaurant_id_seq OWNED BY restaurant.id;


--
-- TOC entry 221 (class 1259 OID 29210)
-- Name: supplier; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE supplier (
    id integer NOT NULL,
    name character varying(255),
    ingredient_id integer
);


ALTER TABLE supplier OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 29208)
-- Name: supplier_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE supplier_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE supplier_id_seq OWNER TO postgres;

--
-- TOC entry 2321 (class 0 OID 0)
-- Dependencies: 220
-- Name: supplier_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE supplier_id_seq OWNED BY supplier.id;


--
-- TOC entry 2103 (class 2604 OID 29096)
-- Name: address id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address ALTER COLUMN id SET DEFAULT nextval('address_id_seq'::regclass);


--
-- TOC entry 2104 (class 2604 OID 29107)
-- Name: bank_transaction id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank_transaction ALTER COLUMN id SET DEFAULT nextval('bank_transaction_id_seq'::regclass);


--
-- TOC entry 2105 (class 2604 OID 29118)
-- Name: client id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client ALTER COLUMN id SET DEFAULT nextval('client_id_seq'::regclass);


--
-- TOC entry 2106 (class 2604 OID 29129)
-- Name: employee id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY employee ALTER COLUMN id SET DEFAULT nextval('employee_id_seq'::regclass);


--
-- TOC entry 2107 (class 2604 OID 29140)
-- Name: ingredient id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ingredient ALTER COLUMN id SET DEFAULT nextval('ingredient_id_seq'::regclass);


--
-- TOC entry 2108 (class 2604 OID 29151)
-- Name: ingredient_recipe id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ingredient_recipe ALTER COLUMN id SET DEFAULT nextval('ingredient_recipe_id_seq'::regclass);


--
-- TOC entry 2109 (class 2604 OID 29159)
-- Name: ingredient_restaurant id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ingredient_restaurant ALTER COLUMN id SET DEFAULT nextval('ingredient_restaurant_id_seq'::regclass);


--
-- TOC entry 2110 (class 2604 OID 29167)
-- Name: order_cart id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_cart ALTER COLUMN id SET DEFAULT nextval('order_cart_id_seq'::regclass);


--
-- TOC entry 2111 (class 2604 OID 29178)
-- Name: order_item id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_item ALTER COLUMN id SET DEFAULT nextval('order_item_id_seq'::regclass);


--
-- TOC entry 2112 (class 2604 OID 29186)
-- Name: order_modification id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_modification ALTER COLUMN id SET DEFAULT nextval('order_modification_id_seq'::regclass);


--
-- TOC entry 2113 (class 2604 OID 29194)
-- Name: recipe id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recipe ALTER COLUMN id SET DEFAULT nextval('recipe_id_seq'::regclass);


--
-- TOC entry 2114 (class 2604 OID 29205)
-- Name: restaurant id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY restaurant ALTER COLUMN id SET DEFAULT nextval('restaurant_id_seq'::regclass);


--
-- TOC entry 2115 (class 2604 OID 29213)
-- Name: supplier id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY supplier ALTER COLUMN id SET DEFAULT nextval('supplier_id_seq'::regclass);

--
-- TOC entry 2322 (class 0 OID 0)
-- Dependencies: 196
-- Name: address_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('address_id_seq', 1, false);


--
-- TOC entry 2323 (class 0 OID 0)
-- Dependencies: 198
-- Name: bank_transaction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('bank_transaction_id_seq', 1, false);


--
-- TOC entry 2324 (class 0 OID 0)
-- Dependencies: 200
-- Name: client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('client_id_seq', 1, false);


--
-- TOC entry 2325 (class 0 OID 0)
-- Dependencies: 202
-- Name: employee_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('employee_id_seq', 1, false);


--
-- TOC entry 2326 (class 0 OID 0)
-- Dependencies: 204
-- Name: ingredient_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ingredient_id_seq', 1, false);


--
-- TOC entry 2327 (class 0 OID 0)
-- Dependencies: 206
-- Name: ingredient_recipe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ingredient_recipe_id_seq', 1, false);


--
-- TOC entry 2328 (class 0 OID 0)
-- Dependencies: 208
-- Name: ingredient_restaurant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('ingredient_restaurant_id_seq', 1, false);


--
-- TOC entry 2329 (class 0 OID 0)
-- Dependencies: 210
-- Name: order_cart_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('order_cart_id_seq', 1, false);


--
-- TOC entry 2330 (class 0 OID 0)
-- Dependencies: 212
-- Name: order_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('order_item_id_seq', 1, false);


--
-- TOC entry 2331 (class 0 OID 0)
-- Dependencies: 214
-- Name: order_modification_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('order_modification_id_seq', 1, false);


--
-- TOC entry 2332 (class 0 OID 0)
-- Dependencies: 216
-- Name: recipe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('recipe_id_seq', 1, false);


--
-- TOC entry 2333 (class 0 OID 0)
-- Dependencies: 218
-- Name: restaurant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('restaurant_id_seq', 1, false);


--
-- TOC entry 2334 (class 0 OID 0)
-- Dependencies: 220
-- Name: supplier_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('supplier_id_seq', 1, false);


--
-- TOC entry 2117 (class 2606 OID 29101)
-- Name: address address_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);


--
-- TOC entry 2119 (class 2606 OID 29112)
-- Name: bank_transaction bank_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank_transaction
    ADD CONSTRAINT bank_transaction_pkey PRIMARY KEY (id);


--
-- TOC entry 2121 (class 2606 OID 29123)
-- Name: client client_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client
    ADD CONSTRAINT client_pkey PRIMARY KEY (id);


--
-- TOC entry 2123 (class 2606 OID 29134)
-- Name: employee employee_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY employee
    ADD CONSTRAINT employee_pkey PRIMARY KEY (id);


--
-- TOC entry 2125 (class 2606 OID 29145)
-- Name: ingredient ingredient_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ingredient
    ADD CONSTRAINT ingredient_pkey PRIMARY KEY (id);


--
-- TOC entry 2127 (class 2606 OID 29153)
-- Name: ingredient_recipe ingredient_recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ingredient_recipe
    ADD CONSTRAINT ingredient_recipe_pkey PRIMARY KEY (id);


--
-- TOC entry 2129 (class 2606 OID 29161)
-- Name: ingredient_restaurant ingredient_restaurant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ingredient_restaurant
    ADD CONSTRAINT ingredient_restaurant_pkey PRIMARY KEY (id);


--
-- TOC entry 2131 (class 2606 OID 29172)
-- Name: order_cart order_cart_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_cart
    ADD CONSTRAINT order_cart_pkey PRIMARY KEY (id);


--
-- TOC entry 2133 (class 2606 OID 29180)
-- Name: order_item order_item_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_item
    ADD CONSTRAINT order_item_pkey PRIMARY KEY (id);


--
-- TOC entry 2135 (class 2606 OID 29188)
-- Name: order_modification order_modification_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_modification
    ADD CONSTRAINT order_modification_pkey PRIMARY KEY (id);


--
-- TOC entry 2137 (class 2606 OID 29199)
-- Name: recipe recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recipe
    ADD CONSTRAINT recipe_pkey PRIMARY KEY (id);


--
-- TOC entry 2139 (class 2606 OID 29207)
-- Name: restaurant restaurant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY restaurant
    ADD CONSTRAINT restaurant_pkey PRIMARY KEY (id);


--
-- TOC entry 2141 (class 2606 OID 29215)
-- Name: supplier supplier_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT supplier_pkey PRIMARY KEY (id);


--
-- TOC entry 2155 (class 2606 OID 29281)
-- Name: supplier fk1wd501d74ixbwxd4p6achjvd4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY supplier
    ADD CONSTRAINT fk1wd501d74ixbwxd4p6achjvd4 FOREIGN KEY (ingredient_id) REFERENCES ingredient(id);


--
-- TOC entry 2148 (class 2606 OID 29246)
-- Name: order_cart fk2b4c13k5nc8nl147aoxwqicsu; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_cart
    ADD CONSTRAINT fk2b4c13k5nc8nl147aoxwqicsu FOREIGN KEY (client_id) REFERENCES client(id);


--
-- TOC entry 2149 (class 2606 OID 29251)
-- Name: order_cart fk3nxqc4e2tc7qtq860rvaq9jia; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_cart
    ADD CONSTRAINT fk3nxqc4e2tc7qtq860rvaq9jia FOREIGN KEY (restaurant_id) REFERENCES restaurant(id);


--
-- TOC entry 2152 (class 2606 OID 29266)
-- Name: order_modification fk4bd7u0cg0uxpj0c6mbli6c4q7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_modification
    ADD CONSTRAINT fk4bd7u0cg0uxpj0c6mbli6c4q7 FOREIGN KEY (ingredient_id) REFERENCES ingredient(id);


--
-- TOC entry 2154 (class 2606 OID 29276)
-- Name: restaurant fk96q13p1ptpewvus590a8o83xt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY restaurant
    ADD CONSTRAINT fk96q13p1ptpewvus590a8o83xt FOREIGN KEY (address_id) REFERENCES address(id);


--
-- TOC entry 2143 (class 2606 OID 29221)
-- Name: client fkb137u2cl2ec0otae32lk5pcl2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY client
    ADD CONSTRAINT fkb137u2cl2ec0otae32lk5pcl2 FOREIGN KEY (address_id) REFERENCES address(id);


--
-- TOC entry 2147 (class 2606 OID 29241)
-- Name: ingredient_restaurant fkb3tvuc1ilbobssii729l5m99d; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ingredient_restaurant
    ADD CONSTRAINT fkb3tvuc1ilbobssii729l5m99d FOREIGN KEY (restaurant_id) REFERENCES restaurant(id);


--
-- TOC entry 2150 (class 2606 OID 29256)
-- Name: order_item fkc6x3tg3ix5owcx7qp84wp2sd4; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_item
    ADD CONSTRAINT fkc6x3tg3ix5owcx7qp84wp2sd4 FOREIGN KEY (order_id) REFERENCES order_cart(id);


--
-- TOC entry 2142 (class 2606 OID 29216)
-- Name: bank_transaction fkdlln58tb27fougns9xn99h55j; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY bank_transaction
    ADD CONSTRAINT fkdlln58tb27fougns9xn99h55j FOREIGN KEY (order_id) REFERENCES order_cart(id);


--
-- TOC entry 2144 (class 2606 OID 29226)
-- Name: ingredient_recipe fkhlx76kitv08yj3x85mtxpkgpf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ingredient_recipe
    ADD CONSTRAINT fkhlx76kitv08yj3x85mtxpkgpf FOREIGN KEY (ingredient_id) REFERENCES ingredient(id);


--
-- TOC entry 2151 (class 2606 OID 29261)
-- Name: order_item fkkjnfpqc22msjnbp9wpnhglvkf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_item
    ADD CONSTRAINT fkkjnfpqc22msjnbp9wpnhglvkf FOREIGN KEY (recipe_id) REFERENCES recipe(id);


--
-- TOC entry 2146 (class 2606 OID 29236)
-- Name: ingredient_restaurant fkp7pd1wmt6j8d2esf7u4o1f8l9; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ingredient_restaurant
    ADD CONSTRAINT fkp7pd1wmt6j8d2esf7u4o1f8l9 FOREIGN KEY (ingredient_id) REFERENCES ingredient(id);


--
-- TOC entry 2145 (class 2606 OID 29231)
-- Name: ingredient_recipe fkqaihl4ckxrrpr33lcu6865rcl; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY ingredient_recipe
    ADD CONSTRAINT fkqaihl4ckxrrpr33lcu6865rcl FOREIGN KEY (recipe_id) REFERENCES recipe(id);


--
-- TOC entry 2153 (class 2606 OID 29271)
-- Name: order_modification fkqfsuaypts156e8sp6i8plwrg7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY order_modification
    ADD CONSTRAINT fkqfsuaypts156e8sp6i8plwrg7 FOREIGN KEY (order_item_id) REFERENCES order_item(id);


-- Completed on 2019-04-08 19:29:47

--
-- PostgreSQL database dump complete
--

